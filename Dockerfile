FROM golang:1.21 AS build

ADD . /src
WORKDIR /src
RUN go build -ldflags='-extldflags=-static -w -s' -tags osusergo,netgo -o /modsec-logger .

FROM scratch
COPY --from=build /modsec-logger /modsec-logger

ENTRYPOINT ["/modsec-logger"]

