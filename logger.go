// Tool to rewrite mod_security2 logs (very difficult to parse
// although they are in semi-structured format) to JSON.
package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strings"
)

var (
	needle       = []byte("ModSecurity: ")
	identifierRx = regexp.MustCompile(`^[a-z][-_a-z]+$`)
)

// Unescape a string escaped by Apache, by simply removing one level
// of backslashes.
func unescapeApache(b []byte) []byte {
	var i, j int
	var quote bool
	for i < len(b) {
		c := b[i]
		i++

		if quote {
			quote = false
		} else if c == '\\' {
			quote = true
			continue
		}

		b[j] = c
		j++
	}
	return b[:j]
}

// Buffer used by the alert parser.
type parsebuf struct {
	buf []byte
	pos int
	sz  int
}

func (p *parsebuf) debugString() string {
	return fmt.Sprintf("<parsebuf:\"%s\">", p.buf[p.pos:])
}

func newParsebuf(b []byte) *parsebuf {
	return &parsebuf{
		buf: b,
		sz:  len(b),
	}
}

// Consume the next character and expect it to be 'c'.
func (p *parsebuf) expect(c byte) error {
	if p.pos >= p.sz {
		return io.EOF
	}
	if p.buf[p.pos] != c {
		return fmt.Errorf("expected '%c', found '%c' at pos %d", c, p.buf[p.pos], p.pos)
	}
	p.pos++
	return nil
}

// Consume the next character and return it.
func (p *parsebuf) next() (byte, error) {
	if p.pos >= p.sz {
		return 0, io.EOF
	}
	c := p.buf[p.pos]
	p.pos++
	return c, nil
}

// Consume until the delimiter (which is discarded).
func (p *parsebuf) until(delim byte) (string, error) {
	start := p.pos
	for p.pos < p.sz && p.buf[p.pos] != delim {
		p.pos++
	}
	if p.pos >= p.sz {
		return "", fmt.Errorf("found EOF looking for '%c'", delim)
	}
	s := string(p.buf[start:p.pos])
	p.pos++
	return s, nil
}

// Consume a string, possibly quoted, until the specified delimiter.
func (p *parsebuf) stringUntil(delim byte) (string, error) {
	if p.pos >= p.sz {
		return "", io.EOF
	}

	// Peek
	if p.buf[p.pos] == '"' {
		s, err := p.quotedStringUntil(delim)
		if err != nil {
			return "", err
		}
		return s, p.expect(delim)
	}
	return p.until(delim)
}

// Consume a quoted string. The given delimiter is expected to
// immediately follow the string.
func (p *parsebuf) quotedStringUntil(delim byte) (string, error) {
	// Walk over first quote.
	if err := p.expect('"'); err != nil {
		return "", err
	}

	// Read and unquote as we go.
	var s string
	var esc = false
	for {
		c, err := p.next()
		if err != nil {
			return "", err
		}

		if esc {
			esc = false
		} else {
			switch c {
			case '\\':
				esc = true
			case '"':
				return s, nil
			}
		}
		s += string(c)
	}
}

type tag struct {
	name  string
	value string
}

// Parse a metadata tag [key value].
func (p *parsebuf) parseTag() (*tag, error) {
	if err := p.expect('['); err != nil {
		return nil, fmt.Errorf("%w while looking for tag start", err)
	}

	name, err := p.until(' ')
	if err != nil {
		return nil, fmt.Errorf("%w while reading tag name", err)
	}

	if !identifierRx.MatchString(name) {
		return nil, fmt.Errorf("invalid identifier '%s'", name)
	}

	value, err := p.stringUntil(']')
	if err != nil {
		return nil, fmt.Errorf("%w while reading tag value", err)
	}

	return &tag{name: name, value: value}, nil
}

// Parse a list of metadata tags (space-separated).
func (p *parsebuf) parseTagList() ([]*tag, error) {
	var l []*tag
	for {
		t, err := p.parseTag()
		if err != nil {
			return nil, err
		}
		l = append(l, t)

		c, err := p.next()
		if errors.Is(err, io.EOF) {
			return l, nil
		}
		if err != nil {
			return nil, err
		}
		switch c {
		case ' ':
		case ',':
			// The comma can introduce trailing data, stop.
			return l, nil
		default:
			return nil, fmt.Errorf("unexpected trailing data in tag list: %s", p.debugString())
		}
	}
}

// Parse a ModSecurity alert message, as documented in
// https://github.com/SpiderLabs/ModSecurity/wiki/ModSecurity-2-Data-Formats
// Returns the "why" message, and the metadata tags.
func (p *parsebuf) parseModSecAlert() (string, []*tag, error) {
	// Since we can't reliably tell the beginning of the metadata
	// section, start at successive positions of '[' until there
	// are no errors.
	start := p.pos
	head := 0
	for {
		n := bytes.IndexByte(p.buf[start+head:], '[')
		if n < 0 {
			return "", nil, errors.New("no metadata section found")
		}

		p.pos = start + head + n
		s := string(p.buf[start : start+head+n])

		tags, err := p.parseTagList()
		if err == nil {
			return strings.TrimSpace(s), tags, err
		}

		head += n + 1
	}
}

// Remove these tags from the generated JSON: these have too high
// cardinality or size, and their values are not particularly
// meaningful for cross-referencing either.
var tagsToIgnore = []string{
	"client", "unique_id", "file", "line",
}

func shouldIgnoreTag(s string) bool {
	for _, t := range tagsToIgnore {
		if s == t {
			return true
		}
	}
	return false
}

var errNotModSec = errors.New("not modsec")

// Parse a ModSecurity alert log message.
func parseModSec(line []byte) (map[string]interface{}, error) {
	n := bytes.Index(line, needle)
	if n < 0 {
		return nil, errNotModSec
	}

	line = unescapeApache(line[n+len(needle):])

	why, tags, err := newParsebuf(line).parseModSecAlert()
	if err != nil {
		return nil, err
	}

	fields := make(map[string]interface{})
	fields["why"] = why

	for _, t := range tags {
		if shouldIgnoreTag(t.name) {
			continue
		}

		// Dynamically convert multi-value tags to lists.
		cur, ok := fields[t.name]
		if ok {
			switch cur := cur.(type) {
			case string:
				fields[t.name] = []string{cur, t.value}
			case []string:
				fields[t.name] = append(cur, t.value)
			}
		} else {
			fields[t.name] = t.value
		}
	}

	return fields, nil
}

func writeLine(w io.Writer, line []byte) error {
	if _, err := w.Write(line); err != nil {
		return err
	}
	_, err := io.WriteString(w, "\n")
	return err
}

func writeModSecAlert(w io.Writer, fields map[string]interface{}) error {
	data, _ := json.Marshal(fields)
	_, err := fmt.Fprintf(w, "@cee:{\"modsec\":%s}\n", data)
	return err
}

func main() {
	outw := os.Stdout

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Bytes()
		meta, err := parseModSec(line)
		if err == nil {
			err = writeModSecAlert(outw, meta)
		} else {
			if !errors.Is(err, errNotModSec) {
				fmt.Fprintf(outw, "modsec_logger parse error: %v\n", err)
			}
			err = writeLine(outw, line)
		}

		if err != nil {
			log.Fatal(err)
		}
	}
}
