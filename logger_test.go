package main

import (
	"bufio"
	"strings"
	"testing"
)

var testLogs = `[Mon Sep 18 11:24:47.943785 2023] [security2:error] [pid 3285584:tid 140521129375424] [client 109.74.204.123:39358] [client 109.74.204.123] ModSecurity: Warning. Operator EQ matched 0 at REQUEST_HEADERS. [file "/usr/share/modsecurity-crs/rules/REQUEST-920-PROTOCOL-ENFORCEMENT.conf"] [line "550"] [id "920280"] [msg "Request Missing a Host Header"] [severity "WARNING"] [ver "OWASP_CRS/3.3.4"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-protocol"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "capec/1000/210/272"] [tag "PCI/6.5.10"] [hostname "www.example.com"] [uri "/"] [unique_id "ZQgzfxlwm0CPl8kXcRpyRAAAAFY"]
[Mon Sep 18 00:08:54.696664 2023] [security2:error] [pid 3285584:tid 140522723202752] [client 205.185.127.161:54454] [client 205.185.127.161] ModSecurity: Warning. Pattern match "^[\\\\d.:]+$" at REQUEST_HEADERS:Host. [file "/usr/share/modsecurity-crs/rules/REQUEST-920-PROTOCOL-ENFORCEMENT.conf"] [line "736"] [id "920350"] [msg "Host header is a numeric IP address"] [data "78.46.196.100"] [severity "WARNING"] [ver "OWASP_CRS/3.3.4"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-protocol"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "capec/1000/210/272"] [tag "PCI/6.5.10"] [hostname "78.46.196.100"] [uri "/"] [unique_id "ZQeVFhlwm0CPl8kXcRpwbgAAAEM"], referer: https://affgate.top/landing/aff/
[Mon Sep 18 13:26:01.435058 2023] [security2:error] [pid 3285576:tid 140522901411520] [remote 205.169.39.105:58479] [client 205.169.39.105] ModSecurity: Warning. Pattern match "^[\\\\d.:]+$" at REQUEST_HEADERS:Host. [file "/usr/share/modsecurity-crs/rules/REQUEST-920-PROTOCOL-ENFORCEMENT.conf"] [line "736"] [id "920350"] [msg "Host header is a numeric IP address"] [data "78.46.196.100"] [severity "WARNING"] [ver "OWASP_CRS/3.3.4"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-protocol"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "capec/1000/210/272"] [tag "PCI/6.5.10"] [hostname "78.46.196.100"] [uri "/style-mini.css"] [unique_id "ZQhP6SlJ_xSHvr7OsEwTrgAABBA"], referer: https://78.46.196.100/
[Mon Sep 18 12:19:02.713886 2023] [security2:error] [pid 3285584:tid 140522152761024] [client 207.90.244.10:56338] [client 207.90.244.10] ModSecurity: Warning. Pattern match "^[\\\\d.:]+$" at REQUEST_HEADERS:Host. [file "/usr/share/modsecurity-crs/rules/REQUEST-920-PROTOCOL-ENFORCEMENT.conf"] [line "736"] [id "920350"] [msg "Host header is a numeric IP address"] [data "78.46.196.100"] [severity "WARNING"] [ver "OWASP_CRS/3.3.4"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-protocol"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "capec/1000/210/272"] [tag "PCI/6.5.10"] [hostname "78.46.196.100"] [uri "/"] [unique_id "ZQhANhlwm0CPl8kXcRpymQAAAE4"]
[Mon Sep 18 09:49:32.840002 2023] [security2:error] [pid 3285584:tid 140521120982720] [client 104.197.141.11:44806] [client 104.197.141.11] ModSecurity: Warning. Operator GE matched 5 at TX:inbound_anomaly_score. [file "/usr/share/modsecurity-crs/rules/RESPONSE-980-CORRELATION.conf"] [line "92"] [id "980130"] [msg "Inbound Anomaly Score Exceeded (Total Inbound Score: 5 - SQLI=0,XSS=0,RFI=0,LFI=0,RCE=0,PHPI=0,HTTP=0,SESS=0): individual paranoia level scores: 5, 0, 0, 0"] [ver "OWASP_CRS/3.3.4"] [tag "event-correlation"] [hostname "www.example.com"] [uri "/"] [unique_id "ZQgdLBlwm0CPl8kXcRpyMwAAAFc"]
[Mon Sep 18 11:24:50.741416 2023] [security2:error] [pid 3285576:tid 140522817484480] [client 109.74.204.123:54886] [client 109.74.204.123] ModSecurity: Warning. Operator GE matched 5 at TX:inbound_anomaly_score. [file "/usr/share/modsecurity-crs/rules/RESPONSE-980-CORRELATION.conf"] [line "92"] [id "980130"] [msg "Inbound Anomaly Score Exceeded (Total Inbound Score: 5 - SQLI=0,XSS=0,RFI=0,LFI=0,RCE=0,PHPI=0,HTTP=0,SESS=0): individual paranoia level scores: 5, 0, 0, 0"] [ver "OWASP_CRS/3.3.4"] [tag "event-correlation"] [hostname "giap.example.com"] [uri "/scripts/WPnBr.dll"] [unique_id "ZQgzgilJ_xSHvr7OsEwTZQAAAAA"]
[Mon Sep 18 11:24:48.760569 2023] [security2:error] [pid 3285584:tid 140521666246336] [client 109.74.204.123:45326] [client 109.74.204.123] ModSecurity: Warning. Operator EQ matched 0 at REQUEST_HEADERS. [file "/usr/share/modsecurity-crs/rules/REQUEST-920-PROTOCOL-ENFORCEMENT.conf"] [line "550"] [id "920280"] [msg "Request Missing a Host Header"] [severity "WARNING"] [ver "OWASP_CRS/3.3.4"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-protocol"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "capec/1000/210/272"] [tag "PCI/6.5.10"] [hostname "www.example.com"] [uri "/"] [unique_id "ZQgzgBlwm0CPl8kXcRpyUgAAAE8"]
[Mon Sep 18 08:07:00.928723 2023] [security2:error] [pid 3285576:tid 140522295371456] [client 162.243.147.4:35956] [client 162.243.147.4] ModSecurity: Warning. Matched phrase "zgrab" at REQUEST_HEADERS:User-Agent. [file "/usr/share/modsecurity-crs/rules/REQUEST-913-SCANNER-DETECTION.conf"] [line "55"] [id "913100"] [msg "Found User-Agent associated with security scanner"] [data "Matched Data: zgrab found within REQUEST_HEADERS:User-Agent: mozilla/5.0 zgrab/0.x"] [severity "CRITICAL"] [ver "OWASP_CRS/3.3.4"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-reputation-scanner"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "capec/1000/118/224/541/310"] [tag "PCI/6.5.10"] [hostname "78.46.196.100"] [uri "/ecp/Current/exporttool/microsoft.exchange.ediscovery.exporttool.application"] [unique_id "ZQgFJClJ_xSHvr7OsEwTBAAAAA0"]
[Mon Sep 18 03:34:08.488024 2023] [security2:error] [pid 3285584:tid 140521615890112] [client 1.202.114.196:27593] [client 1.202.114.196] ModSecurity: Warning. Pattern match "^[\\\\d.:]+$" at REQUEST_HEADERS:Host. [file "/usr/share/modsecurity-crs/rules/REQUEST-920-PROTOCOL-ENFORCEMENT.conf"] [line "736"] [id "920350"] [msg "Host header is a numeric IP address"] [data "78.46.196.100"] [severity "WARNING"] [ver "OWASP_CRS/3.3.4"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-protocol"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "capec/1000/210/272"] [tag "PCI/6.5.10"] [hostname "78.46.196.100"] [uri "/favicon.ico"] [unique_id "ZQfFMBlwm0CPl8kXcRpxYgAAAFU"]
[Mon Sep 18 13:10:29.864739 2023] [security2:error] [pid 3285584:tid 140522739988160] [client 20.172.0.244:52689] [client 20.172.0.244] ModSecurity: Warning. Pattern match "^[\\\\d.:]+$" at REQUEST_HEADERS:Host. [file "/usr/share/modsecurity-crs/rules/REQUEST-920-PROTOCOL-ENFORCEMENT.conf"] [line "736"] [id "920350"] [msg "Host header is a numeric IP address"] [data "78.46.196.100"] [severity "WARNING"] [ver "OWASP_CRS/3.3.4"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-protocol"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "capec/1000/210/272"] [tag "PCI/6.5.10"] [hostname "78.46.196.100"] [uri "/site/wp-includes/wlwmanifest.xml"] [unique_id "ZQhMRRlwm0CPl8kXcRpy7AAAAEE"]
`

func TestUnescape(t *testing.T) {
	s := string(unescapeApache([]byte("hello \\\\world")))
	exp := "hello \\world"
	if s != exp {
		t.Fatalf("unexpected result: '%s'", s)
	}
}

func TestParseTags(t *testing.T) {
	s := "Hello world! [id 42] [path \"/path/to/foo\"]"
	b := newParsebuf([]byte(s))
	msg, tags, err := b.parseModSecAlert()
	if err != nil {
		t.Fatal(err)
	}

	if msg != "Hello world!" {
		t.Fatalf("unexpected message: '%s'", msg)
	}
	if len(tags) != 2 {
		t.Fatalf("did not parse all tags: %+v", tags)
	}
	for _, tag := range tags {
		switch tag.name {
		case "id":
			if tag.value != "42" {
				t.Errorf("unexpected 'id' value '%s'", tag.value)
			}
		case "path":
			if tag.value != "/path/to/foo" {
				t.Errorf("unexpected 'path' value '%s'", tag.value)
			}
		default:
			t.Errorf("unexpected metadata tag '%s'", tag.name)
		}
	}
}

func TestParseModSec(t *testing.T) {
	scanner := bufio.NewScanner(strings.NewReader(testLogs))
	for scanner.Scan() {
		meta, err := parseModSec(scanner.Bytes())
		if err != nil {
			t.Errorf("failed on line '%s': %v", scanner.Text(), err)
		}
		t.Logf("%+v", meta)
	}

}
