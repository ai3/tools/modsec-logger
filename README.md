modsec-logger
===

Simple tool to rewrites <a
href="https://www.modsecurity.org/">ModSecurity</a> V2 alerts into a
JSON format more suitable for ingestion and analysis in a centralized
logging system.

The tool is meant to be used as a pass-through for Apache logs, e.g.

```
ErrorLog "|/usr/bin/modsec-logger"
```

it will pass through all its input to standard output, with the
exception of ModSecurity alerts that are parsed in their [original
format](https://github.com/SpiderLabs/ModSecurity/wiki/ModSecurity-2-Data-Formats)
and rewritten to Lumberjack-compliant JSON (i.e. JSON with a `@cee:`
prefix).

This was considered the simplest option, an alternative could have
been to implement an *audit log collector* (ModSecurity audit logs
have access to the full request and response, as well as more verbose
matching information).
